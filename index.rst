.. Magia Break Pseudepigrapha documentation master file, created by
   sphinx-quickstart on Fri Aug  5 15:24:39 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

マギブレ設定資料集
=====================

Contents:

.. toctree::
   :maxdepth: 2
   :glob:

   character/*
   world/*

.. Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
