========
タマコ
========

**「にゃはは！　ちょっとそこで引っ掛けちゃって！」**

**「そっかぁ……。お友達になりたかったのにな〜。」**

**「友達、いっぱいかぁ〜♪　どうやったらできるかな〜？」**

-------
三行で
-------

生まれながらに「周囲に不幸を撒き散らす力」を持つ悪魔。

幼さからか自分の力を理解しておらず、友達を作ろうとしては不幸に陥れることを繰り返してきた。

魔王軍でもその力は発揮され、タマコの世話を命じられたメイドにも降りかかることになり……。

------------
基本データ
------------

* 身長：低い
* 体型：痩せ気味
* 出典：日本の民間伝承(猫又)
* 目的：友達がいっぱい欲しい
* 反応傾向：楽観的
* 活動性：能動的
* 出生地：
* 誕生方法：
* 好きなこと・もの：
* 苦手なこと・もの：

------------------
その他、ネタとか
------------------

　メインストーリーに絡まない、いわゆる普通のガチャ登場キャラです。二尾の猫悪魔ということで元ネタは猫又でしょう(資料に明記されてなかったけど)。猫又→ネコマタ→(逆転させて)タマコネ→タマコ。

　「周囲に不幸を撒き散らす力」は一定範囲内にいる相手なら無差別にはたらきますが、魔王や魔王直属隊に所属するレベルの力を持つ悪魔であれば難なく抵抗ができる微弱なものでもあります。

　魔王軍なら強力な悪魔がいっぱいいるので友達もできることでしょう。
